angular.module('app', [])

.controller('validarForm', ['$scope', function($scope) {
    $scope.tab = 1;

    $scope.setTab = function(newTab){
      $scope.tab = newTab;
    };

    $scope.isSet = function(tabNum){
      return $scope.tab === tabNum;
    };
	
  }])



//Calendario
	$.datepicker.regional['es'] = {
		 closeText: 'Cerrar',
		 prevText: '',
		 nextText: ' ',
		 currentText: 'Hoy',
		 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
		 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
		 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
		 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
		 weekHeader: 'Sm',
		 dateFormat: 'dd/mm/yy',
		 firstDay: 1,
		 isRTL: false,
		 showMonthAfterYear: false,
		 yearSuffix: ''
	 };
	$.datepicker.setDefaults($.datepicker.regional['es']);
	$(function() {
		$( "#datepicker" ).datepicker({
			firstDay: 1 
		});
	});

$(document).ready(function() {
	$('.modalEjemplo_view').click(function (e) {
		$('#modalEjemplo').modal();
		return false;
	})
	

	// Menu hambuergesa
	$("#effect").toggle(false);
	$("#hamburger").click(function (event) {
		event.stopPropagation();
		 $( "#effect" ).toggle( "slide"); 
	});

	$(document).click(function() {
		$("#effect").toggle(false);
	});
	$("#effect").click (function (event){
		event.stopPropagation();
	}); 

	/*Menu User*/
	$(".MenuUser, .MenuUser1").hide();
	$('.imgShowMenuUser').click(function() {
		$(".MenuUser, .MenuUser1").toggle("ver");
	});

	/********Para selects*******************/
	$( "select").dropkick({
		mobile: true
	});	
});
//Validacion login
$(".txtRojo").hide();
function validaLogin(f) {
	if(f.txtUsuario.value == "")  
  	{	
	  $(".txtRojo").html('Por favor ingrese su usuario');
	  return false;
  	}else
	if(f.txtLlave.value == "" )
  	{
	  $(".txtRojo").html('Por favor ingrese su contraseña');
	  return false;
  	}else
	{
		location.href="index.html";
		return false;}
}
/*Valida buscador del menu de hamburgesa*/
function valida(f) {
	if (f.busca.value == "")  {
		alert("Es necesario que introduzca un valor");
	}else { 
		return false;
	}
}
/*Detecta resolucion de pantalla*/
if (matchMedia) {
  const mq = window.matchMedia("(min-width: 780px)");
  mq.addListener(WidthChange);
  WidthChange(mq);
}
function WidthChange(mq) {
	if (mq.matches) {
	  	$("#menu ul").addClass("normal");
	  	$("#menu ul li").removeClass("in");
		$('ul.nivel1 >li > ul').slideUp();
		$('ul.nivel2 >li > ul').slideUp();
		$('ul.nivel1>li').off("click");
		$('ul.nivel2>li').off("click");
	} else {
	   $("#menu ul").removeClass("normal");

		$('ul.nivel1>li').on('click', function(event) {
			event.stopPropagation();
			
			$target = $(this).children();

			if ($(this).hasClass("in"))  {
			    $('ul.nivel2').slideUp();

				$(this).removeClass("in");
				$('.flecha').removeClass("rotar");
			}else {
			  	$('ul.nivel1 > li').removeClass("in");
				$('ul.nivel2').slideUp();
				$('ul.nivel3').slideUp();
				$('ul.nivel2>li').removeClass("in");
				$(this).addClass("in");
			  	$target.slideDown();
				$('ul.nivel1 > li > a .flecha').addClass("rotar");
				
			}
		});
		$('ul.nivel2>li').on('click', function(event) {
			event.stopPropagation();
		
			$target = $(this).children();

			if ($(this).hasClass("in"))  {
			    $('ul.nivel3').slideUp();
				$(this).removeClass("in");
				$('ul.nivel2 > li > a .flecha').removeClass("rotar");
			}else {
			  	$('ul.nivel2 > li').removeClass("in");
				$('ul.nivel3').slideUp();
				$(this).addClass("in");
			  	$target.slideDown();
				$('ul.nivel2 > li > a .flecha').addClass("rotar");
			}
		});
		$('ul.nivel3>li').on('click', function(event) {
			event.stopPropagation();
		});
	}
}
