// JavaScript Document
angular.module('app', [])
  .controller('validarForm', ['$scope', '$filter', function($scope, $filter) {
	 $scope.prioridades = [
			{ nombre: "alta", countTotal: 0, count: 0, porce: 0 },
			{ nombre: "media", countTotal: 0, count: 0, porce: 0 },
			{ nombre: "baja", countTotal: 0, count: 0, porce: 0 }];
	$scope.estatus1 = ["toDo", "desarrollo", "pruebas", "parcial", "pendiente", "liberado"];
    $scope.tarjetas =
	[
		{noID: 1, estatus:'backlog', 	 prioridad:'alta'},		
		{noID: 2, estatus:'toDo', 	 prioridad:'alta'},		
		{noID: 7, estatus:'desarrollo',  prioridad:'alta'},
		{noID: 8, estatus:'desarrollo',  prioridad:'alta'},
		{noID: 11, estatus:'pruebas', 	 prioridad:'alta'},
		
		{noID: 4, estatus:'toDo',    	 prioridad:'media'},
		{noID: 5, estatus:'backlog',     prioridad:'media'},
		{noID: 6, estatus:'desarrollo',  prioridad:'media'},
		
		{noID: 12, estatus:'backlog', 	 prioridad:'baja'},
		{noID: 13, estatus:'toDo', 	 prioridad:'baja'},
		{noID: 14, estatus:'desarrollo', prioridad:'baja'},
		{noID: 15, estatus:'pruebas', 	 prioridad:'baja'},
	];  
 	for (var i = 0; i < $scope.prioridades.length; i++) {
		var prioridad_var = $scope.prioridades[i].nombre;
		$scope.prioridades[i].countTotal = ($filter('filter')($scope.tarjetas, { prioridad: prioridad_var })).length;
		$scope.prioridades[i].count = $scope.prioridades[i].countTotal - ($filter('filter')($scope.tarjetas, function (tarjeta) {
			return (
				(tarjeta.prioridad === prioridad_var && tarjeta.estatus === 'backlog')
				|| (tarjeta.prioridad === prioridad_var && tarjeta.estatus === 'toDo')
			);
		})).length;
		$scope.prioridades[i].porce = Math.round(($scope.prioridades[i].count * 100) / $scope.prioridades[i].countTotal);
		console.log($scope.prioridades[i]);
	}
 }]);


        
