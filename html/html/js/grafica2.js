new Chart(document.getElementById("mixed-chart2"), {
    type: 'bar',
    data: {
      labels: ["1", "2", "3","4","5","6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],
      datasets: [{
          label: "HC Activo",
          type: "line",
          borderColor: "#8f2104",
          data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20000, 20000, 20000, 20000, 20000, 21000, 21000, 21000, 21000, 21000, 21000, 21000, 21000 ],
          fill: false
        }, {
          label: "HC Autorizado",
          type: "line",
          borderColor: "#04c046",
          data: [17000, 17000, 17000, 17000, 17000, 17000, 17000, 17000, 17000, 17000, 17000, 17000, 17000, 17000, 17000, 17000, 20000, 22000, 22000, 22000, 22000, 22000, 25000, 25000, 25000, 25000, 25000, 25000, 30000, 30000 ],
          fill: false
        }, {
          label: "Nómina Real Bruta",
          type: "bar",
          backgroundColor: color(window.chartColors.yellow1).rgbString(),
          data: [1500, 16000, 15000, 14000, 15000, 14000, 17000, 16000, 19000, 14000, 13000, 18000, 16000, 17000, 16000, 16000, 17000, 20000, 16000, 18000, 17000, 21000, 19000, 23000, 22000, 21000, 19000, 21000, 26000, 32000],
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'Gráfica'
      },
      legend: { display: true},// quita cuadros
      responsive: true,
      legend: {
        position: 'top',
      },
      scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }],
            xAxes: [{
                type: 'category',
                categoryPercentage: 0.9,
                barPercentage: 0.5
            }]
        }
    }
});






